<?php

namespace Drupal\search_api_clir\EventSubscriber;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\locale\StringStorageInterface;
use Drupal\search_api\Plugin\search_api\data_type\value\TextValueInterface;
use Drupal\search_api_solr\Event\PreAddLanguageFallbackFieldEvent;
use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Search API Solr events subscriber.
 */
class SearchApiSolrSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The string storage for reading and writing translations.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  protected $localeStorage;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Creates a new typed configuration manager.
   *
   * @param \Drupal\locale\StringStorageInterface $locale_storage
   *   The locale storage to use for reading string translations.
   */
  public function __construct(StringStorageInterface $locale_storage, LanguageManagerInterface $language_manager) {
    $this->localeStorage = $locale_storage;
    $this->languageManager = $language_manager;
  }

  /**
   * Adds the mapping to treat some Solr special fields as fulltext in views.
   *
   * @param \Drupal\search_api_solr\Event\PreAddLanguageFallbackFieldEvent $event
   *   The Search API Solr event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function preAddLanguageFallbackField(PreAddLanguageFallbackFieldEvent $event): void {
    switch ($event->getType()) {
      case 'solr_text_custom:string':
        // Don't translate "strings".

        break;

      case 'text':
      default:
        $item = $event->getItem();
        if ($item->getLanguage() === $this->languageManager->getDefaultLanguage()->getId()) {
          $index = $event->getIndex();
          $settings = search_api_clir_merge_default_index_third_party_settings($index->getThirdPartySettings('search_api_clir'));
          if (!$settings['enable']) {
            break;
          }

          $langcode = $event->getLangcode();
          $original_value = $event->getValue();
          $value = is_object($original_value) ? clone $original_value : $original_value;

          foreach ($value as $key => $original_text_value) {
            if ($original_text_value instanceof TextValueInterface) {
              $text_value = clone $original_text_value;

              $text = $text_value->getText();
              // Previous processing steps like 'auto_aggregated_fulltext_field'
              // might already have triggered a translation.
              if (!($text instanceof TranslatableMarkup)) {
                if ($translation_source = $this->localeStorage->findString([
                  'source' => $text,
                  'language' => $langcode,
                  'translated' => TRUE
                ])) {
                  // There's an existing translation from any context. We simply
                  // use it.
                  $text_value->setText($this->t($text, [], [
                    'langcode' => $langcode,
                    'context' => $translation_source->context,
                  ])->render());

                  $value[$key] = $text_value;
                }
                else {
                  // The context identifier has a maximum length of 255
                  // characters. The item ID is limited to 150 characters, a UUID
                  // has 36 characters. So the concatination should be safe.
                  $context = 'search_api_clir;' . $event->getIndex()
                      ->uuid() . '|' . $item->getId();
                  $translation_sources = $this->localeStorage->getStrings([
                    'source' => $text,
                    'language' => $langcode,
                    'translated' => FALSE
                  ]);
                  foreach ($translation_sources as $translation_source) {
                    if ($translation_source->context && strpos($translation_source->context, 'search_api_clir;') === 0) {
                      // The text is already stored as translation source by
                      // search_api_clir but not translated yet.
                      // Leave the text untranslated and continue with the next
                      continue 2;

                      // @todo If the translation is stored in a different
                      //   search_api_clir context, store the current context
                      //   somewhere else as related. That will allow to
                      //   trigger re-indexing for the different item as well
                      //   when the translation arrives.
                    }
                  }

                  // The text does not exist as translation source within a
                  // search_api_clir context. Register it to let tgmt translate
                  // it, but keep the untranslated text for current indexing.
                  $this->t($text, [], [
                    'langcode' => $langcode,
                    'context' => $context,
                  ])->render();
                }
              }
            }
          }

          $event->setValue($value);
        }

        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SearchApiSolrEvents::PRE_ADD_LANGUAGE_FALLBACK_FIELD => 'preAddLanguageFallbackField',
    ];

  }

}
