<?php

namespace Drupal\search_api_clir;

/**
 * The job manager interface.
 */
interface JobManagerInterface {

  /**
   * @param bool|null $auto_accept
   *   Whether to accept translations autmatically or not, default is true.
   * @param int|null $limit
   *   The limit of translations.
   * @param string|null $target_langcode
   *   The target language, all languages if NULL.
   * @param string|null $index_id
   *   Limit translations to this Search API index.
   */
  public function requestTranslations(?bool $auto_accept = NULL, ?int $limit = NULL, ?string $target_langcode = NULL, ?string $index_id = NULL): void;

  /**
   * Gets locale strings.
   *
   * @param string|null $search_label
   *   Label to search for.
   * @param string|null $missing_target_language
   *   Missing translation language.
   * @param string|null $context
   *   The translation context.
   * @param int|null $limit
   *   The maximum number of strings.
   *
   * @return array
   *   List of i18n strings data.
   */
  public function loadStrings(?string $search_label = NULL, ?string $missing_target_language = NULL, ?string $context = 'search_api_clir', ?int $limit = NULL): array;

}
