<?php

namespace Drupal\search_api_clir;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt_locale\LocaleSourcePluginUi;

/**
 * Locale source plugin UI.
 *
 * Plugin UI for i18n strings translation jobs.
 */
class SearchApiClirSourcePluginUi extends LocaleSourcePluginUi {

  /**
   * {@inheritdoc}
   */
  function getStrings($search_label = NULL, $missing_target_language = NULL, $context = NULL) {
    /** @var \Drupal\search_api_clir\JobManagerInterface $jobManager */
    $jobManager = \Drupal::service('search_api_clir.job_manager');
    return $jobManager->loadStrings($search_label, $missing_target_language, $context, \Drupal::config('tmgmt.settings')->get('source_list_limit', 20));
  }

  /**
   * {@inheritdoc}
   */
  public function overviewSearchFormPart(array $form, FormStateInterface $form_state, $type): array {
    $form = parent::overviewSearchFormPart($form, $form_state, $type);

    $form['search_wrapper']['search']['context'] = [
      '#type' => 'hidden',
      '#default_value' => 'search_api_clir',
    ];

    return $form;
  }

  /**
   * Gets a list of contexts in the system.
   */
  public function getContexts(): array {
    return ['search_api_clir', 'search_api_clir'];
  }

  /**
   * Gets submitted search params.
   *
   * @return array
   */
  public function getSearchFormSubmittedParams() {
    $params = parent::getSearchFormSubmittedParams();

    $params['context'] = 'search_api_clir';

    return $params;
  }

}
