<?php

namespace Drupal\search_api_clir\Plugin\tmgmt\Source;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Utility\Utility;
use Drupal\tmgmt\ContinuousSourceInterface;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt_locale\Plugin\tmgmt\Source\LocaleSource;

/**
 * Translation Source plugin for locale strings.
 *
 * @SourcePlugin(
 *   id = "search_api_clir",
 *   label = @Translation("Search API CLIR"),
 *   description = @Translation("Source handler for locale strings created by Search API CLIR."),
 *   ui = "Drupal\search_api_clir\SearchApiClirSourcePluginUi"
 * )
 */
class SearchApiClirSource extends LocaleSource implements ContinuousSourceInterface {

  /**
   * {@inheritdoc}
   */
  public function getItemTypes() {
    return array('default' => t('Indexed Search API language fallback fields'));
  }

  /**
   * Updates translation associated to a specific locale source.
   *
   * @param string $lid
   *   The Locale ID.
   * @param string $target_language
   *   Target language to update translation.
   * @param string $translation
   *   Translation value.
   *
   * @return bool
   *   Success or not updating the locale translation.
   */
  protected function updateTranslation($lid, $target_language, $translation) {
    $success = parent::updateTranslation($lid, $target_language, $translation);

    if ($success) {
      $context = \Drupal::database()->query("SELECT context FROM {locales_source} WHERE lid = :lid", [
        ':lid' => $lid,
      ])->fetchField();

      if (strpos($context, 'search_api_clir;') === 0) {
        // Mark item as modified in Search API tracker.
        [$index_uuid, $combined_id] = explode('|', str_replace('search_api_clir;', '', $context));
        [$datasource_id, $item_id] = Utility::splitCombinedId($combined_id);

        /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository */
        $entityRepository = \Drupal::service('entity.repository');
        try {
          if ($index = $entityRepository->loadEntityByUuid('search_api_index', $index_uuid)) {
            $index->trackItemsUpdated($datasource_id, [$item_id]);
            // @todo re-index related items as well.
          }
        } catch (EntityStorageException $e) {
          // Skip this item.
        }
      }
    }

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function continuousSettingsForm(array &$form, FormStateInterface $form_state, Job $job) {
    $continuous_settings = $job->getContinuousSettings();

    $element['enabled'] = [
      '#type' => 'checkbox',
      '#title' => 'Search API CLIR',
      '#default_value' => $continuous_settings[$this->getPluginId()]['enabled'] ?? FALSE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateContinuousItem(Job $job, $plugin, $item_type, $item_id) {
    $continuous_settings = $job->getContinuousSettings();

    // @todo double check if $item_id ($lid) has no translation for
    //   $job->getTargetLangcode().

    return TRUE;
  }


}
