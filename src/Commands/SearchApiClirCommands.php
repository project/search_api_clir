<?php

namespace Drupal\search_api_clir\Commands;

use Drupal\search_api_clir\JobManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Defines Drush commands for the Search API Solr.
 */
class SearchApiClirCommands extends DrushCommands {

  /**
   * The job manager.
   *
   * @var \Drupal\search_api_clir\JobManagerInterface
   */
  protected $jobManager;

  /**
   * Constructs a SearchApiClirCommands object.
   *
   * @param \Drupal\search_api_clir\JobManagerInterface $languageManager
   *   The job manager.
   */
  public function __construct(JobManagerInterface $jobManager) {
    parent::__construct();
    $this->jobManager = $jobManager;
  }

  /**
   * Request translations.
   *
   * @param array $options
   *   The options array.
   *
   * @command search-api-clir:request-translations
   *
   * @option language Request translations for a specific language.
   * @option limit Limit the number of translation requests.
   * @option auto-accept Automatically accept requested translations.
   * @option index Limit translations to an index.
   *
   * @default $options []
   *
   * @usage drush search-api-clir:request-translations
   *   Request all pending translations.
   * @usage drush search-api-clir:request-translations --auto-accept
   *   Request all pending translations and automatically accept provided
   *   translations.
   * @usage drush search-api-clir:request-translations --language=fr --limit=100
   *   Request next 100 pending translations for target language 'fr'.
   *
   * @aliases clir-rt
   */
  public function requestTranslations(array $options = ['language' => NULL, 'limit' => NULL, 'auto-accept' => FALSE, 'index' => NULL]): void {
    $this->jobManager->requestTranslations((bool) $options['auto-accept'], $options['limit'] === NULL ? -1 : (int) $options['limit'], $options['language'], $options['index']);
  }

}
