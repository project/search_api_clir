<?php

namespace Drupal\search_api_clir;

use Drupal\Core\Config\ConfigException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\IndexInterface;
use Drupal\tmgmt\ContinuousManager;

/**
 * Defines Drush commands for the Search API Solr.
 */
class JobManager implements JobManagerInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The TMGMT ContinuousManager.
   *
   * @var \Drupal\tmgmt\ContinuousManager
   */
  protected $continuousManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a JobManager object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\tmgmt\ContinuousManager $continuousManager
   *   The continuousManager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *    The config factory.
   */
  public function __construct(LanguageManagerInterface $languageManager, ContinuousManager $continuousManager, ConfigFactoryInterface $config_factory) {
    $this->languageManager = $languageManager;
    $this->continuousManager = $continuousManager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslations(?bool $auto_accept = NULL, ?int $limit = NULL, ?string $target_langcode = NULL, ?string $index_id = NULL): void {
    if (!$this->configFactory->get('tmgmt.settings')->get('submit_job_item_on_cron')) {
      throw new ConfigException('search_api_clir requires submit_job_item_on_cron to be set in tmgmt.settings.');
    }

    $indexes = [];
    if ($index_id) {
      $indexes[] = Index::load($index_id);
    }
    else {
      $indexes = Index::loadMultiple();
    }

    if ($jobs = $this->continuousManager->getContinuousJobs($this->languageManager->getDefaultLanguage()->getId())) {
      /** @var \Drupal\tmgmt\Entity\Job $job */
      foreach ($jobs as $job) {
        if ($target_langcode && $target_langcode !== $job->getTargetLangcode()) {
          continue;
        }

        foreach ($indexes as $index) {
          if ($index instanceof IndexInterface && $index->isServerEnabled() && !$index->isReadOnly()) {
            $settings = search_api_clir_merge_default_index_third_party_settings($index->getThirdPartySettings('search_api_clir'));
            if ($settings['enable']) {
              $strings = $this->loadStrings(NULL, $job->getTargetLangcode(), 'search_api_clir;' . $index->uuid(), $limit !== NULL ? $limit : $settings['cron']['limit']);
              foreach ($strings as $string) {
                /** @var \Drupal\tmgmt\Entity\JobItem $item */
                $this->continuousManager->addItem($job, 'search_api_clir', 'locale', $string->lid);
              }

              if (($auto_accept && $limit) || ($auto_accept === NULL && $settings['cron']['auto_accept'] && $settings['cron']['limit'])) {
                $job->acceptTranslation();
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadStrings(?string $search_label = NULL, ?string $missing_target_language = NULL, ?string $context = 'search_api_clir', ?int $limit = NULL): array {
    if ($limit === 0) {
      return [];
    }

    $langcodes = array_keys($this->languageManager->getLanguages());
    $languages = array_combine($langcodes, $langcodes);
    $select = \Drupal::database()->select('locales_source', 'ls')
      ->fields('ls', ['lid', 'source', 'context']);

    if (!empty($search_label)) {
      $select->condition('ls.source', '%' . \Drupal::database()->escapeLike($search_label) . '%', 'LIKE');
    }
    if (!empty($context)) {
      $select->condition('ls.context', \Drupal::database()->escapeLike($context) . '%', 'LIKE');
    }
    if (!empty($missing_target_language) && in_array($missing_target_language, $languages)) {
      $select->isNull("lt_$missing_target_language.language");
    }

    // Join locale targets for each language.
    // We want all joined fields to be named as langcodes, but langcodes could
    // contain hyphens in their names, which is not allowed by the most database
    // engines. So we create a langcode-to-filed_alias map, and rename fields
    // later.
    $langcode_to_filed_alias_map = [];
    foreach ($languages as $langcode) {
      $table_alias = $select->leftJoin('locales_target', \Drupal::database()->escapeTable("lt_$langcode"), "ls.lid = %alias.lid AND %alias.language = '$langcode'");
      $langcode_to_filed_alias_map[$langcode] = $select->addField($table_alias, 'language');
    }
    unset($field_alias);

    if ($limit && $limit !== -1) {
      $select = $select->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit($limit);
    }

    $rows = $select->execute()->fetchAll();

    foreach ($rows as $row) {
      foreach ($langcode_to_filed_alias_map as $langcode => $field_alias) {
        $row->{$langcode} = $row->{$field_alias};
        unset($row->{$field_alias});
      }
    }
    unset($row);

    return $rows;
  }
}
