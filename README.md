Search API CLIR (cross-language information retrieval)
======================================================

If some content is not translated into all languages, Search API is able to index the
original language instead. That will add that content to search results even if they're
filtered by that targeted language the content is not translated for.
This works great for all searches that don't include fulltext searches.
If you search for _schwarz_ or _noir(e)_ in German or French content you'll obviously
won't find the English fallback content if its text contains _black_.

That's the problem CLIR solves!

If we assume that you can read the fallback language but you search in the current
interface language, it would be fine to have the fallback as part of the search result.
But to achieve that we need to index the translation that does not exist. CLIR assumes
that the result of machine translation (Google, Microsoft, DeepL) is good enough for that
purpose. So Search API CLIR will add machine translations to every indexed content that
that is marked as fallback for another missing translation.

Usage
-----

- add a TMGMT "Continuous Job" and activate "Search API CLIR" for it for every language
  you want to target
- add a "Language (with fallback)" field to your index
- enable CLIR in your index edit form
- re-index
- run `drush clir-rt --auto-accept` or wait for cron if enabled for CLIR in your index
  edit form
- (re-index)

Caveats
-------

- Search API CLIR currently only works with the Search API Solr backend as other backends
  don't support multiple language-specific fields in one record in parallel.
- The fallback needs to be the site's default language.
- Re-indexing is not yet triggered automatically for every content if a (mashine)
  translation is added and accepted.
